<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 *
 * @UniqueEntity(fields={"email", "username"}, message="This one is already taken")
 */
class User implements UserInterface
{
    /**
     * @ORM\Id()
     *
     * @ORM\GeneratedValue()
     *
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(name="username", type="string", length=50)
     *
     * @Assert\NotBlank
     */
    private $username;

    /**
     * @ORM\Column(name="password", type="string", length=255)
     *
     * @Assert\NotBlank
     */
    private $password;

    /**
     * @ORM\Column(name="email", type="string", length=255)
     *
     * @Assert\Email()
     *
     * @Assert\NotBlank
     */
    private $email;

    /**
     * @ORM\Column(name="fullname", type="string", length=50, nullable=true)
     */
    private $fullname;

    /**
     * @ORM\OneToMany(targetEntity="Team", mappedBy="author")
     *
     * @ORM\JoinColumn(name="author_id", referencedColumnName="id")
     */
    private $projectAuthor;

    /**
     * @ORM\OneToMany(targetEntity="Task", mappedBy="developer")
     *
     * @ORM\JoinColumn(name="developer_id", referencedColumnName="id")
     */
    private $taskDev;

    /**
     * @ORM\OneToMany(targetEntity="Team", mappedBy="participant")
     *
     * @ORM\JoinColumn(name="participant_id", referencedColumnName="id")
     */
    private $team;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    public $path;

    /**
     * @Assert\File
     */
    protected $file;

    /**
     * @ORM\Column(type="boolean")
     */
    private $enable = true;

    /**
     * @ORM\OneToMany(targetEntity=Comment::class, mappedBy="author")
     */
    private $comments;

    public function __construct()
    {
        $this->taskDev = new ArrayCollection();
        $this->team = new ArrayCollection();
        $this->comments = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @return User
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @return User
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @return User
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    public function getFullname()
    {
        return $this->fullname;
    }

    /**
     * @return User
     */
    public function setFullname($fullname)
    {
        $this->fullname = $fullname;

        return $this;
    }

    /**
     * Returns the roles granted to the user.
     *
     * <code>
     * public function getRoles()
     * {
     *     return array('ROLE_USER');
     * }
     * </code>
     *
     * Alternatively, the roles might be stored on a ``roles`` property,
     * and populated in any number of different ways when the user object
     * is created.
     *
     * @return (Role|string)[] The user roles
     */
    public function getRoles()
    {
        return [
            'ROLE_USER',
        ];
    }

    /**
     * @return Team[]|ArrayCollection
     */
    public function getProjectAuthor()
    {
        return $this->projectAuthor;
    }

    public function setProjectAuthor(ArrayCollection $projectAuthor): User
    {
        $this->projectAuthor = $projectAuthor;

        return $this;
    }

    public function getTaskDev(): ArrayCollection
    {
        return $this->taskDev;
    }

    public function setTaskDev(ArrayCollection $taskDev): User
    {
        $this->taskDev = $taskDev;

        return $this;
    }

    public function getTeam(): ArrayCollection
    {
        return $this->team;
    }

    public function setTeam(ArrayCollection $team): User
    {
        $this->team = $team;

        return $this;
    }

    public function getPath()
    {
        return $this->path;
    }

    /**
     * @return User
     */
    public function setPath($path)
    {
        $this->path = $path;

        return $this;
    }

    public function getFile()
    {
        return $this->file;
    }

    /**
     * @return User
     */
    public function setFile($file)
    {
        $this->file = $file;

        return $this;
    }

    public function getEnable()
    {
        return $this->enable;
    }

    /**
     * @return User
     */
    public function setEnable($enable)
    {
        $this->enable = $enable;

        return $this;
    }

    public function getAbsolutePath()
    {
        return null === $this->path
            ? null
            : $this->getUploadRootDir().'/'.$this->path;
    }

    public function getWebPath()
    {
        return null === $this->path
            ? null
            : $this->getUploadDir().'/'.$this->path;
    }

    protected function getUploadRootDir()
    {
        // the absolute directory path where uploaded
        // documents should be saved
        return __DIR__.'/../../public/'.$this->getUploadDir();
    }

    protected function getUploadDir()
    {
        // get rid of the __DIR__ so it doesn't screw up
        // when displaying uploaded doc/image in the view.
        return 'avatars';
    }

    public function upload()
    {
        // the file property can be empty if the field is not required
        if (null === $this->getFile()) {
            return;
        }

        // use the original file name here but you should
        // sanitize it at least to avoid any security issues

        $filename = md5($this->getFile()->getClientOriginalName().$this->getUsername().time()).'.png';

        // move takes the target directory and then the
        // target filename to move to
        $this->getFile()->move(
            $this->getUploadRootDir(),
            $filename
        );

        // set the path property to the filename where you've saved the file
        //        $this->path = $this->getFile()->getClientOriginalName();
        $this->path = $filename;

        // clean up the file property as you won't need it anymore
        $this->file = null;
    }

    /**
     * Returns the salt that was originally used to encode the password.
     *
     * This can return null if the password was not encoded using a salt.
     *
     * @return string|null The salt
     */
    public function getSalt()
    {
        // TODO: Implement getSalt() method.
    }

    /**
     * Removes sensitive data from the user.
     *
     * This is important if, at any given point, sensitive information like
     * the plain-text password is stored on this object.
     */
    public function eraseCredentials()
    {
        // TODO: Implement eraseCredentials() method.
    }

    public function __toString()
    {
        return (string) $this->getUsername();
    }

    /**
     * @return Collection|Comment[]
     */
    public function getComments(): Collection
    {
        return $this->comments;
    }

    public function addComment(Comment $comment): self
    {
        if (!$this->comments->contains($comment)) {
            $this->comments[] = $comment;
            $comment->setAuthor($this);
        }

        return $this;
    }

    public function removeComment(Comment $comment): self
    {
        if ($this->comments->contains($comment)) {
            $this->comments->removeElement($comment);
            // set the owning side to null (unless already changed)
            if ($comment->getAuthor() === $this) {
                $comment->setAuthor(null);
            }
        }

        return $this;
    }
}
