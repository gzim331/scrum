<?php

namespace App\Entity;

use App\Traits\EntityBaseTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProjectRepository")
 */
class Project
{
    use EntityBaseTrait;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Assert\NotBlank(message="This field cannot be blank")
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="Task", mappedBy="project")
     */
    private $task;

    /**
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\OneToMany(targetEntity="Category", mappedBy="project")
     */
    private $category;

    /**
     * @ORM\OneToMany(targetEntity="Team", mappedBy="project")
     * @ORM\JoinColumn(name="project_id", referencedColumnName="id")
     */
    private $team;

    /**
     * @ORM\OneToMany(targetEntity=Board::class, mappedBy="project")
     */
    private $boards;

    public function __construct()
    {
        $this->task = new ArrayCollection();
        $this->category = new ArrayCollection();
        $this->team = new ArrayCollection();
        $this->boards = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     *
     * @return Project
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    public function getTask(): ArrayCollection
    {
        return $this->task;
    }

    public function setTask(ArrayCollection $task): Project
    {
        $this->task = $task;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     *
     * @return Project
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    public function getCategory(): ArrayCollection
    {
        return $this->category;
    }

    public function setCategory(ArrayCollection $category): Project
    {
        $this->category = $category;

        return $this;
    }

    public function getTeam(): Collection
    {
        return $this->team;
    }

    public function setTeam(ArrayCollection $team): Project
    {
        $this->team = $team;

        return $this;
    }

    public function __toString()
    {
        return (string) $this->getName();
    }

    /**
     * @return Collection|Board[]
     */
    public function getBoards(): Collection
    {
        return $this->boards;
    }

    public function addBoard(Board $board): self
    {
        if (!$this->boards->contains($board)) {
            $this->boards[] = $board;
            $board->setProject($this);
        }

        return $this;
    }

    public function removeBoard(Board $board): self
    {
        if ($this->boards->contains($board)) {
            $this->boards->removeElement($board);
            // set the owning side to null (unless already changed)
            if ($board->getProject() === $this) {
                $board->setProject(null);
            }
        }

        return $this;
    }
}
