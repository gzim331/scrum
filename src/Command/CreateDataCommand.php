<?php

namespace App\Command;

use App\Entity\Category;
use App\Entity\Project;
use App\Entity\Task;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class CreateDataCommand extends Command
{
    protected static $defaultName = 'app:create:data';
    private $entityManager;

    public function __construct(
        EntityManagerInterface $entityManager,
        string $name = null
    ) {
        parent::__construct($name);
        $this->entityManager = $entityManager;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        foreach ($this->entityManager->getRepository(Project::class)->findAll() as $project) {
            for ($i = 1; $i <= 3; ++$i) {
                $category = (new Category())
                    ->setProject($project)
                    ->setName('Category'.$i)
                    ->setCreatedAt(new \DateTime());
                $this->entityManager->persist($category);

                foreach ($project->getBoards() as $board) {
                    for ($i = 1; $i <= 6; ++$i) {
                        $task = (new Task())
                            ->setProject($board->getProject())
                            ->setDescription('Lorem ipsum')
                            ->setBoard($board)
                            ->setTitle('Task'.$i.$board->getName())
                            ->setCategory($category)
                            ->setCreatedAt(new \DateTime())
                            ->setStatus(Task::STATUS_ICEBOX);
                        $this->entityManager->persist($task);
                    }
                }
            }
            $this->entityManager->flush();
        }

        $this->entityManager->flush();

        $io = new SymfonyStyle($input, $output);
        $io->success('OK');

        return 0;
    }
}
