<?php

namespace App\Repository;

use App\Entity\Board;
use App\Entity\Project;
use App\Entity\Task;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Task|null find($id, $lockMode = null, $lockVersion = null)
 * @method Task|null findOneBy(array $criteria, array $orderBy = null)
 * @method Task[]    findAll()
 * @method Task[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TaskRepository extends ServiceEntityRepository
{
    /**
     * @var CategoryRepository
     */
    private $categoryRepository;

    public function __construct(ManagerRegistry $registry, CategoryRepository $categoryRepository)
    {
        parent::__construct($registry, Task::class);
        $this->categoryRepository = $categoryRepository;
    }

    public function findTasksByCategory($category)
    {
        $query = $this->createQueryBuilder('task')
            ->andWhere('task.category = :category')
            ->setParameter('category', $category);
        $this->undeletedParam($query);

        return $query->getQuery()->getResult();
    }

    public function findTasksByProject(Project $project)
    {
        $query = $this->createQueryBuilder('task')
            ->orderBy('task.createdAt', 'DESC');
        $this->undeletedParam($query);
        $this->taskByProject($query, $project);

        return $query->getQuery()->getResult();
    }

    public function iceBox(Project $project, Board $board, $developer = null, $category = null)
    {
        return $this->findTasksByStatus($project, Task::STATUS_ICEBOX, $board, $developer, $category);
    }

    public function inProgress(Project $project, Board $board = null, $developer = null, $category = null)
    {
        return $this->findTasksByStatus($project, Task::STATUS_INPROGRESS, $board, $developer, $category);
    }

    public function testing(Project $project, Board $board, $developer = null, $category = null)
    {
        return $this->findTasksByStatus($project, Task::STATUS_TESTING, $board, $developer, $category);
    }

    public function complete(Project $project, Board $board, $developer = null, $category = null)
    {
        return $this->findTasksByStatus($project, Task::STATUS_COMPLETE, $board, $developer, $category);
    }

    private function undeletedParam($query)
    {
        return $query
            ->andWhere('task.deleted = :deleted')
            ->setParameter('deleted', false);
    }

    private function taskByProject($query, $project)
    {
        return $query
            ->andWhere('task.project = :project')
            ->setParameter('project', $project->getId());
    }

    private function findTasksByStatus($project, $status, $board, $developer, $category)
    {
        $query = $this->createQueryBuilder('task')
            ->andWhere('task.status = :status')
            ->setParameter('status', $status);

        if (null !== $board) {
            $query->andWhere('task.board = :board')
                ->setParameter('board', $board);
        }

        $this->taskByProject($query, $project);
        $this->undeletedParam($query);

        if ($developer && 'All' != $developer) {
            $this->findByDeveloper($query, $developer);
        }

        if ($category && 'All' != $category) {
            $this->findByCategory($query, $category, $project);
        }

        return $query->getQuery()->getResult();
    }

    private function findByDeveloper($query, $developer)
    {
        $user = $this->getEntityManager()->getRepository(User::class)->findOneBy(['username' => $developer, 'enable' => true]);

        if ($user) {
            return $query
                ->andWhere('task.developer = :developer')
                ->setParameter('developer', $user);
        }
    }

    private function findByCategory($query, $categoryName, $project)
    {
        $category = $this->categoryRepository->findCategoryByName($categoryName, $project);

        if ($category) {
            return $query
                ->andWhere('task.category = :category')
                ->setParameter('category', $category);
        }
    }

    public function findTasksByBoard(Board $board)
    {
        $query = $this->createQueryBuilder('task')
            ->andWhere('task.board = :board')
            ->setParameter('board', $board);
        $this->undeletedParam($query);

        return $query->getQuery()->getResult();
    }

    public function findByBacklog(Project $project)
    {
        $query = $this->createQueryBuilder('task')
            ->leftJoin('task.board', 'board')
            ->andWhere('board.name = :name')
            ->setParameter('name', 'Backlog');
        $this->taskByProject($query, $project);
        $this->undeletedParam($query);

        return $query->getQuery()->getResult();
    }
}
