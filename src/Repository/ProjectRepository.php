<?php

namespace App\Repository;

use App\Entity\Project;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\Request;

/**
 * @method Project|null find($id, $lockMode = null, $lockVersion = null)
 * @method Project|null findOneBy(array $criteria, array $orderBy = null)
 * @method Project[]    findAll()
 * @method Project[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProjectRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Project::class);
    }

    /**
     * @return mixed
     */
    public function selectAllProject(User $user)
    {
        $q = $this->createQueryBuilder('project')
            ->leftJoin('project.team', 'team')
            ->andWhere('team.participant = :participant')
            ->setParameter('participant', $user->getId())
            ->andWhere('team.deleted = :status')
            ->setParameter('status', false)
            ->addOrderBy('project.modified', 'DESC')
            ->addOrderBy('project.createdAt', 'DESC');
        $this->undeletedParam($q);

        return $q->getQuery()->getResult();
    }

    /**
     * @return mixed
     */
    public function selectUserProject(User $user)
    {
        $query = $this->createQueryBuilder('project')
            ->leftJoin('project.team', 'team')
            ->andWhere('team.participant = :participant')
            ->setParameter('participant', $user->getId())
            ->andWhere('team.deleted = :deleted')
            ->setParameter('deleted', false)
            ->andWhere('team.author = :author')
            ->setParameter('author', $user)
            ->addOrderBy('project.modified', 'DESC')
            ->addOrderBy('project.createdAt', 'DESC');
        $this->undeletedParam($query);

        return $query->getQuery()->getResult();
    }

    /**
     * @return mixed
     */
    public function searchProject(Request $request)
    {
        $q = $this->createQueryBuilder('project')
            ->andWhere('project.name LIKE :search')
            ->setParameter('search', '%'.$request->get('search_project').'%')
            ->orderBy('project.name', 'ASC');
        $this->undeletedParam($q);

        return $q->getQuery()->getResult();
    }

    private function undeletedParam($query)
    {
        $query
            ->andWhere('project.deleted = :deleted')
            ->setParameter('deleted', false);
    }
}
