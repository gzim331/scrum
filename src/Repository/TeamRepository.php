<?php

namespace App\Repository;

use App\Entity\Project;
use App\Entity\Team;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Team|null find($id, $lockMode = null, $lockVersion = null)
 * @method Team|null findOneBy(array $criteria, array $orderBy = null)
 * @method Team[]    findAll()
 * @method Team[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TeamRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Team::class);
    }

    public function selectTeam(Project $project)
    {
        $qb = $this->createQueryBuilder('team')
            ->andWhere('team.project = :project')
            ->andWhere('team.invitation = :invitation')
            ->andWhere('team.deleted = :deleted')
            ->setParameter('project', $project)
            ->setParameter('invitation', false)
            ->setParameter('deleted', false);
        $this->undeletedTeam($qb);

        return $qb->getQuery()->getResult();
    }

    public function findProjectByAuthor($user)
    {
        $query = $this->createQueryBuilder('team')
            ->andWhere('team.author = :user')
            ->leftJoin('team.project', 'project')
            ->andWhere('project.deleted = :deleted')
            ->setParameter('user', $user)
            ->setParameter('deleted', false)
            ->addOrderBy('project.modified', 'DESC')
            ->addOrderBy('project.createdAt', 'DESC');
        $this->undeletedTeam($query);

        return $query->getQuery()->getResult();
    }

    public function selectRequest(Project $project)
    {
        $qb = $this->createQueryBuilder('team')
            ->andWhere('team.project = :project')
            ->andWhere('team.invitation = :invitation')
            ->setParameter('project', $project)
            ->setParameter('invitation', true);
        $this->undeletedTeam($qb);

        return $qb->getQuery()->getResult();
    }

    private function undeletedTeam($query)
    {
        return $query
            ->andWhere('team.deleted = :deleted')
            ->setParameter('deleted', false);
    }

    public function hasPermissions($project, $user)
    {
        $query = $this->createQueryBuilder('team')
            ->andWhere('team.project = :project')
            ->andWhere('team.participant = :participant')
            ->setParameter('project', $project)
            ->setParameter('participant', $user);
        $this->undeletedTeam($query);

        return $query->getQuery()->getOneOrNullResult();
    }

    private function getParticipantProject($project, $user)
    {
        $query = $this->createQueryBuilder('team')
            ->andWhere('team.project = :project')
            ->andWhere('team.participant = :participant')
            ->setParameter('project', $project)
            ->setParameter('participant', $user);
        $this->undeletedTeam($query);

        return $query;
    }

    public function isStatusDeveloper($project, $user)
    {
        $query = $this->getParticipantProject($project, $user)
            ->andWhere('team.permission = :permission')
            ->setParameter('permission', Team::STATUS_ROLE_DEVELOPER);

        return $query->getQuery()->getOneOrNullResult();
    }

    public function isStatusModerator($project, $user)
    {
        $query = $this->getParticipantProject($project, $user)
            ->andWhere('team.permission = :permission')
            ->setParameter('permission', Team::STATUS_ROLE_MODERATOR);

        return $query->getQuery()->getOneOrNullResult();
    }

    public function isStatusMaster($project, $user)
    {
        $query = $this->getParticipantProject($project, $user)
            ->andWhere('team.permission = :permission')
            ->setParameter('permission', Team::STATUS_ROLE_MASTER);

        return $query->getQuery()->getOneOrNullResult();
    }
}
