<?php

namespace App\Repository;

use App\Entity\Category;
use App\Entity\Project;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Category|null find($id, $lockMode = null, $lockVersion = null)
 * @method Category|null findOneBy(array $criteria, array $orderBy = null)
 * @method Category[]    findAll()
 * @method Category[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CategoryRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Category::class);
    }

    /**
     * @return mixed
     */
    public function findCategoryByProject(Project $project)
    {
        return $this->createQueryBuilder('category')
            ->andWhere('category.project = :project')
            ->andWhere('category.deleted = :category_deleted')
            ->leftJoin('category.project', 'project')
            ->andWhere('project.deleted = :project_deleted')
            ->setParameter('project', $project)
            ->setParameter('category_deleted', false)
            ->setParameter('project_deleted', false)
            ->orderBy('category.name', 'ASC')
            ->getQuery()->getResult();
    }

    public function findCategoryByName($name, $project)
    {
        return $this->createQueryBuilder('category')
            ->andWhere('category.name = :name')
            ->andWhere('category.deleted = :deleted')
            ->andWhere('category.project = :project')
            ->setParameter('name', $name)
            ->setParameter('deleted', false)
            ->setParameter('project', $project)
            ->getQuery()->getResult();
    }
}
