<?php

namespace App\Repository;

use App\Entity\Board;
use App\Entity\Project;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Board|null find($id, $lockMode = null, $lockVersion = null)
 * @method Board|null findOneBy(array $criteria, array $orderBy = null)
 * @method Board[]    findAll()
 * @method Board[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BoardRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Board::class);
    }

    public function findBoardByProject(Project $project, $hidden = true)
    {
        $query = $this->createQueryBuilder('board')
            ->andWhere('board.project = :project')
            ->andWhere('board.deleted = :board_deleted')
            ->leftJoin('board.project', 'project')
            ->andWhere('project.deleted = :project_deleted')
            ->setParameter('project', $project)
            ->setParameter('board_deleted', false)
            ->setParameter('project_deleted', false);

        if (false === $hidden) {
            $query->andWhere('board.hidden = :hidden')
                ->setParameter('hidden', $hidden);
        }

        return $query->orderBy('board.sequence', 'DESC')
            ->addOrderBy('board.name', 'ASC')
            ->getQuery()->getResult();
    }

    public function checkBoardExist($name, $project)
    {
        return $this->findOneBy(['project' => $project, 'name' => $name]) ? true : false;
    }

    public function isBoardUndeleted($name, $project)
    {
        return $this->findOneBy(['project' => $project, 'name' => $name, 'deleted' => false]) ? true : false;
    }
}
