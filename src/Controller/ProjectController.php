<?php

namespace App\Controller;

use App\Entity\Board;
use App\Entity\Project;
use App\Entity\Team;
use App\Form\ProjectType;
use App\Repository\BoardRepository;
use App\Repository\CategoryRepository;
use App\Repository\ProjectRepository;
use App\Repository\TaskRepository;
use App\Repository\TeamRepository;
use App\Service\PaginationService;
use App\Service\PermissionService;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\Annotation\Route;

class ProjectController extends AbstractController
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var PermissionService
     */
    private $permission;
    /**
     * @var PaginationService
     */
    private $pagination;
    /**
     * @var CategoryRepository
     */
    private $categoryRepository;
    /**
     * @var RequestStack
     */
    private $requestStack;
    /**
     * @var ProjectRepository
     */
    private $projectRepository;
    /**
     * @var TaskRepository
     */
    private $taskRepository;
    /**
     * @var TeamRepository
     */
    private $teamRepository;
    /**
     * @var BoardRepository
     */
    private $boardRepository;

    public function __construct(
        EntityManagerInterface $entityManager,
        PermissionService $permission,
        PaginationService $pagination,
        CategoryRepository $categoryRepository,
        RequestStack $requestStack,
        ProjectRepository $projectRepository,
        TaskRepository $taskRepository,
        TeamRepository $teamRepository,
        BoardRepository $boardRepository
    ) {
        $this->entityManager = $entityManager;
        $this->permission = $permission;
        $this->pagination = $pagination;
        $this->categoryRepository = $categoryRepository;
        $this->requestStack = $requestStack->getCurrentRequest();
        $this->projectRepository = $projectRepository;
        $this->taskRepository = $taskRepository;
        $this->teamRepository = $teamRepository;
        $this->boardRepository = $boardRepository;
    }

    /**
     * @Route("/", name="all_project")
     * @Template("project/index.html.twig")
     *
     * @return array
     */
    public function index()
    {
        $selectProject = $this->projectRepository->selectAllProject($this->getUser());

        return [
            'selectProject' => $this->pagination->paginate($selectProject, $this->requestStack),
        ];
    }

    /**
     * @Route("/personal/{username}", name="your_project")
     * @Template("project/by_user.html.twig")
     *
     * @return array
     */
    public function byUser()
    {
        $selectProject = $this->teamRepository->findProjectByAuthor($this->getUser());

        return [
            'selectProject' => $this->pagination->paginate($selectProject, $this->requestStack),
        ];
    }

    /**
     * @Route("/{name}/dashboard", name="dashboard_project")
     * @Template("project/dashboard.html.twig")
     *
     * @return array|\Symfony\Component\HttpFoundation\Response
     */
    public function dashboard(Project $project, Team $team)
    {
        if ($this->projectRepository->findOneBy(['name' => $project->getName(), 'deleted' => true])) {
            throw new \Exception("Project doesn't exist");
        }

        $userInProject = $this->userInProject($this->getUser(), $project);

        if ('STATUS_NONE' === $userInProject) {
            return $this->render('project/initial_dashboard.html.twig', [
                'project' => $project,
            ]);
        } elseif ('STATUS_INVITATION' === $userInProject) {
            return $this->render('project/waiting_dashboard.html.twig', [
                'project' => $project,
                'team' => $team,
                'message' => $this->teamRepository->findOneBy([
                    'participant' => $this->getUser(),
                    'project' => $project,
                ])->getMessage(),
            ]);
        }

        return [
            'project' => $project,
            'team' => $team,
            'selectTeam' => $this->teamRepository->selectTeam($project),
            'selectBoard' => $this->boardRepository->findBoardByProject($project, false),
            'inProgress' => count($this->taskRepository->inProgress($project)),
            'backlog' => count($this->taskRepository->findByBacklog($project)),
        ];
    }

    private function userInProject($user, $project)
    {
        if (!$this->teamRepository->findOneBy(['participant' => $user, 'project' => $project, 'deleted' => false])) {
            return 'STATUS_NONE';
        } elseif ($this->teamRepository->findOneBy(['participant' => $user, 'project' => $project, 'invitation' => true, 'deleted' => false])) {
            return 'STATUS_INVITATION';
        }
    }

    /**
     * @Route("/project/new", name="new_project")
     * @Template("project/new_project.html.twig")
     *
     * @return null[]|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function newProject()
    {
        $formProject = $this->createForm(ProjectType::class);

        if ($this->requestStack->isMethod('POST')) {
            $formProject->handleRequest($this->requestStack);
            if ($formProject->isValid()) {
                $data = $formProject->getData();
                if ($this->projectRepository->findOneBy(['name' => $data->getName(), 'deleted' => false])) {
                    $this->addFlash('alert', "Project couldn't be created");
                } else {
                    if ($project = $this->projectRepository->findOneBy(['name' => $data->getName(), 'deleted' => true])) {
                        $this->setProjectValues($project, $data);

                        $team = new Team();
                        $this->setTeamValuesDuringCreateProject($team, $project);
                    } else {
                        $project = new Project();
                        $this->setProjectValues($project, $data);

                        $team = new Team();
                        $this->setTeamValuesDuringCreateProject($team, $project);
                    }

                    $board = new Board();
                    $this->setDefaultBoardValues($board, $project);

                    $this->entityManager->persist($project);
                    $this->entityManager->persist($team);
                    $this->entityManager->persist($board);
                    $this->entityManager->flush();
                }

                return $this->redirectToRoute('dashboard_project', [
                    'name' => $project->getName(),
                ]);
            } else {
                $this->addFlash('alert', "Project couldn't be created");
            }
        }

        return [
            'formProject' => isset($formProject) ? $formProject->createView() : null,
        ];
    }

    private function setDefaultBoardValues($board, $project)
    {
        $board->setName('Backlog')
            ->setSequence(PHP_INT_MAX)
            ->setHidden(false)
            ->setStatus(Board::STATUS_RED)
            ->setProject($project)
            ->setDeleted(false)
            ->setCreatedAt(new \DateTime())
            ->setModified(null);
    }

    private function setProjectValues($project, $data)
    {
        $project->setName($data->getName())
            ->setDescription($data->getDescription())
            ->setDeleted(false)
            ->setCreatedAt(new \DateTime())
            ->setModified(null);
    }

    private function setTeamValuesDuringCreateProject($team, $project)
    {
        $team->setParticipant($this->getUser())
            ->setProject($project)
            ->setAuthor($this->getUser())
            ->setInvitation(false)
            ->setCreatedAt(new \DateTime())
            ->setPermission(Team::STATUS_ROLE_MASTER);
    }

    /**
     * @Route("/project_delete/{id}", name="delete_project")
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function delete(Project $project)
    {
        $this->permission->checkPermissions($project, Team::STATUS_ROLE_MASTER);

        $project->setDeleted(true);
        $this->entityManager->persist($project);

        foreach ($this->teamRepository->findBy(['project' => $project, 'deleted' => false]) as $team) {
            $team->setDeleted(true);
            $this->entityManager->persist($team);
        }
        foreach ($this->taskRepository->findBy(['project' => $project, 'deleted' => false]) as $task) {
            $task->setDeleted(true);
            $this->entityManager->persist($task);
        }
        foreach ($this->categoryRepository->findBy(['project' => $project, 'deleted' => false]) as $category) {
            $category->setDeleted(true);
            $this->entityManager->persist($category);
        }

        $this->entityManager->flush();

        return $this->redirectToRoute('all_project');
    }

    /**
     * @Route("/search", name="search_project")
     * @Template("project/search_project.html.twig")
     *
     * @return array
     */
    public function searchProject()
    {
        $searchProject = $this->projectRepository->searchProject($this->requestStack);

        return [
            'search' => $this->pagination->paginate($searchProject, $this->requestStack),
        ];
    }

    /**
     * @Route("/{name}/settings", name="settings_project")
     * @Template("project/settings.html.twig")
     *
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function settings(Project $project)
    {
        $this->permission->checkPermissions($project, Team::STATUS_ROLE_MASTER);

        $formSettings = $this->createForm(ProjectType::class, $project);
        if ($this->requestStack->isMethod('POST')) {
            $formSettings->handleRequest($this->requestStack);
            $this->entityManager->persist($project);
            $this->entityManager->flush();
            $this->addFlash('success', 'The information has been updated');

            return $this->redirectToRoute('settings_project', ['name' => $project->getName()]);
        }

        return [
            'form' => isset($formSettings) ? $formSettings->createView() : null,
            'project' => $project,
        ];
    }
}
