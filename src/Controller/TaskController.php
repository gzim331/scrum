<?php

namespace App\Controller;

use App\Entity\Board;
use App\Entity\Project;
use App\Entity\Task;
use App\Entity\Team;
use App\Form\TaskType;
use App\Repository\BoardRepository;
use App\Repository\CategoryRepository;
use App\Repository\TaskRepository;
use App\Repository\TeamRepository;
use App\Service\PaginationService;
use App\Service\PermissionService;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\Annotation\Route;

class TaskController extends AbstractController
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var PermissionService
     */
    private $permission;
    /**
     * @var PaginationService
     */
    private $pagination;
    /**
     * @var CategoryRepository
     */
    private $categoryRepository;
    /**
     * @var RequestStack
     */
    private $requestStack;
    /**
     * @var TaskRepository
     */
    private $taskRepository;
    /**
     * @var TeamRepository
     */
    private $teamRepository;
    /**
     * @var BoardRepository
     */
    private $boardRepository;

    public function __construct(
        EntityManagerInterface $entityManager,
        PermissionService $permission,
        PaginationService $pagination,
        CategoryRepository $categoryRepository,
        TaskRepository $taskRepository,
        RequestStack $requestStack,
        TeamRepository $teamRepository,
        BoardRepository $boardRepository
    ) {
        $this->entityManager = $entityManager;
        $this->permission = $permission;
        $this->pagination = $pagination;
        $this->categoryRepository = $categoryRepository;
        $this->requestStack = $requestStack->getCurrentRequest();
        $this->taskRepository = $taskRepository;
        $this->teamRepository = $teamRepository;
        $this->boardRepository = $boardRepository;
    }

    /**
     * @Route("/{name}/task", name="index_task")
     *
     * @Template("task/index.html.twig")
     *
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function index(Project $project)
    {
        $this->permission->checkPermissions($project, Team::STATUS_ROLE_MODERATOR);

        $task = new Task();
        $formTask = $this->createForm(TaskType::class, $task, [
            'project' => $project,
        ]);

        if ($this->requestStack->isMethod('POST')) {
            $formTask->handleRequest($this->requestStack);
            if ($formTask->isValid()) {
                $backlogBoard = $this->boardRepository->findOneBy(['project' => $project, 'name' => 'Backlog']);

                $task->setStatus(Task::STATUS_ICEBOX)
                    ->setCreatedAt(new \DateTime())
                    ->setProject($project)
                    ->setBoard($backlogBoard)
                    ->getProject()->setModified(new \DateTime());
                $this->entityManager->persist($task);
                $this->entityManager->flush();

                return $this->redirectToRoute('index_task', ['name' => $project->getName()]);
            } else {
                $this->addFlash('alert', "Task couldn't be added");
            }
        }

        $resultSelectTask = $this->taskRepository->findTasksByProject($project);

        return [
            'formTask' => isset($formTask) ? $formTask->createView() : null,
            'selectTask' => $this->pagination->paginate($resultSelectTask, $this->requestStack),
            'project' => $project,
        ];
    }

    /**
     * @Route("/edit/task/{id}", name="edit_task")
     *
     * @Template("task/edit.html.twig")
     *
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function edit(Task $task)
    {
        $this->permission->checkPermissions($task->getProject(), Team::STATUS_ROLE_MODERATOR);

        if ($task->getDeleted()) {
            return $this->redirectToRoute('index_task', ['name' => $task->getProject()]);
        }

        $formTask = $this->createForm(TaskType::class, $task, [
            'project' => $task->getProject(),
        ]);
        if ($this->requestStack->isMethod('POST')) {
            $formTask->handleRequest($this->requestStack);
            $task->getProject()->setModified(new \DateTime());
            $task->setModified(new \DateTime());
            $this->entityManager->persist($task);
            $this->entityManager->flush();

            return $this->redirectToRoute('index_task', ['name' => $task->getProject()]);
        }

        return [
            'formTask' => isset($formTask) ? $formTask->createView() : null,
            'project' => $task->getProject(),
        ];
    }

    /**
     * @Route("/task_delete/{id}", name="delete_task")
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function delete(Task $task)
    {
        $this->permission->checkPermissions($task->getProject(), Team::STATUS_ROLE_MODERATOR);

        $task->setDeleted(true)
            ->getProject()->setModified(new \DateTime())
            ->setModified(new \DateTime());
        $this->entityManager->persist($task);
        $this->entityManager->flush();

        return $this->redirectToRoute('index_task', ['name' => $task->getProject()]);
    }

    /**
     * @Route("/{name}/scrum-table/{id}", name="table_task")
     *
     * @Template("task/scrum_table.html.twig")
     *
     * @return array
     */
    public function scrumTable(Board $board)
    {
        $this->permission->checkPermissions($board->getProject(), Team::STATUS_ROLE_DEVELOPER);

        $developer = $this->requestStack->get('developer_filter');
        $category = $this->requestStack->get('category_filter');

        return [
            'project' => $board->getProject(),
            'board' => $board,
            'selectCategory' => $this->categoryRepository->findCategoryByProject($board->getProject()),
            'team' => $this->teamRepository->hasPermissions($board->getProject(), $this->getUser()),
            'teamDevelopers' => $this->teamRepository->selectTeam($board->getProject()),
            'icebox' => $this->taskRepository->iceBox($board->getProject(), $board, $developer, $category),
            'inProgress' => $this->taskRepository->inProgress($board->getProject(), $board, $developer, $category),
            'testing' => $this->taskRepository->testing($board->getProject(), $board, $developer, $category),
            'complete' => $this->taskRepository->complete($board->getProject(), $board, $developer, $category),
        ];
    }

    /**
     * @Route("/set-in-progress/{id}", name="set_inprogress_task")
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function toInProgress(Task $task)
    {
        $this->permission->checkPermissions($task->getProject(), Team::STATUS_ROLE_DEVELOPER);

        $task->setStatus(Task::STATUS_INPROGRESS)
            ->setDeveloper($this->getUser())
            ->getProject()->setModified(new \DateTime())
            ->setModified(new \DateTime());
        $this->entityManager->persist($task);
        $this->entityManager->flush();

        return $this->redirectToRoute('table_task', [
            'name' => $task->getProject(),
            'id' => $task->getBoard()->getId(),
        ]);
    }

    /**
     * @Route("/set-testing/{id}", name="set_testing_task")
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function toTesting(Task $task)
    {
        $this->permission->checkPermissions($task->getProject(), Team::STATUS_ROLE_DEVELOPER);

        $task->setStatus(Task::STATUS_TESTING)
            ->getProject()->setModified(new \DateTime())
            ->setModified(new \DateTime());
        $this->entityManager->persist($task);
        $this->entityManager->flush();

        return $this->redirectToRoute('table_task', [
            'name' => $task->getProject(),
            'id' => $task->getBoard()->getId(),
        ]);
    }

    /**
     * @Route("/set-complete/{id}", name="set_complete_task")
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function toComplete(Task $task)
    {
        $this->permission->checkPermissions($task->getProject(), Team::STATUS_ROLE_DEVELOPER);

        $task->setStatus(Task::STATUS_COMPLETE)
            ->getProject()->setModified(new \DateTime())
            ->setModified(new \DateTime());
        $this->entityManager->persist($task);
        $this->entityManager->flush();

        return $this->redirectToRoute('table_task', [
            'name' => $task->getProject(),
            'id' => $task->getBoard()->getId(),
        ]);
    }

    /**
     * @Route("/resign/{id}", name="resign")
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function resign(Task $task)
    {
        $this->permission->checkPermissions($task->getProject(), Team::STATUS_ROLE_DEVELOPER);

        $task->setStatus(Task::STATUS_ICEBOX)
            ->setDeveloper(null)
            ->getProject()->setModified(new \DateTime())
            ->setModified(new \DateTime());
        $this->entityManager->persist($task);
        $this->entityManager->flush();

        return $this->redirectToRoute('table_task', [
            'name' => $task->getProject(),
            'id' => $task->getBoard()->getId(),
        ]);
    }
}
