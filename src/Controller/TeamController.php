<?php

namespace App\Controller;

use App\Entity\Project;
use App\Entity\Team;
use App\Repository\TeamRepository;
use App\Service\PaginationService;
use App\Service\PermissionService;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\Annotation\Route;

class TeamController extends AbstractController
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var PermissionService
     */
    private $permission;
    /**
     * @var PaginationService
     */
    private $pagination;
    /**
     * @var RequestStack
     */
    private $requestStack;
    /**
     * @var TeamRepository
     */
    private $teamRepository;

    public function __construct(
        EntityManagerInterface $entityManager,
        PermissionService $permission,
        PaginationService $pagination,
        RequestStack $requestStack,
        TeamRepository $teamRepository
    ) {
        $this->entityManager = $entityManager;
        $this->permission = $permission;
        $this->pagination = $pagination;
        $this->requestStack = $requestStack->getCurrentRequest();
        $this->teamRepository = $teamRepository;
    }

    /**
     * @Route("/{name}/team", name="index_team")
     * @Template("team/index.html.twig")
     *
     * @return array
     */
    public function index(Project $project)
    {
        $this->permission->checkPermissions($project, Team::STATUS_ROLE_MASTER);

        $selectTeam = $this->teamRepository->selectTeam($project);

        return [
            'selectTeam' => $this->pagination->paginate($selectTeam, $this->requestStack),
            'project' => $project,
        ];
    }

    /**
     * @Route("/set-master/{id}", name="set_master_team")
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function setProjectMaster(Team $team)
    {
        $this->permission->checkPermissions($team->getProject(), Team::STATUS_ROLE_MASTER);

        $team->setPermission(Team::STATUS_ROLE_MASTER);
        $this->entityManager->persist($team);
        $this->entityManager->flush();

        return $this->redirectToRoute('index_team', ['name' => $team->getProject()]);
    }

    /**
     * @Route("/set-moderator/{id}", name="set_moderator_team")
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function setProjectModerator(Team $team)
    {
        $this->permission->checkPermissions($team->getProject(), Team::STATUS_ROLE_MASTER);

        $team->setPermission(Team::STATUS_ROLE_MODERATOR);
        $this->entityManager->persist($team);
        $this->entityManager->flush();

        return $this->redirectToRoute('index_team', ['name' => $team->getProject()]);
    }

    /**
     * @Route("/set-developer/{id}", name="set_developer_team")
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function setProjectDeveloper(Team $team)
    {
        $this->permission->checkPermissions($team->getProject(), Team::STATUS_ROLE_MASTER);

        $team->setPermission(Team::STATUS_ROLE_DEVELOPER);
        $this->entityManager->persist($team);
        $this->entityManager->flush();

        return $this->redirectToRoute('index_team', ['name' => $team->getProject()->getName()]);
    }

    /**
     * @Route("/remove-team/{id}", name="remove_team")
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     *
     * @throws \Exception
     */
    public function remove(Team $team)
    {
        $this->permission->checkPermissions($team->getProject(), Team::STATUS_ROLE_MASTER);

        $team->setDeleted(true)
            ->setInvitation(false)
            ->setPermission(null)
            ->setMessage('')
            ->setModified(new \DateTime());
        $this->entityManager->persist($team);
        $this->entityManager->flush();

        return $this->redirectToRoute('index_team', ['name' => $team->getProject()]);
    }

    /**
     * @Route("/exit-team/{id}", name="exit_team")
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     *
     * @throws \Exception
     */
    public function exit(Team $team)
    {
        if ($team->getParticipant() != $this->getUser()) {
            throw new \Exception('Access Denied');
        }

        if (Team::STATUS_ROLE_MASTER === $team->getPermission() &&
            count($this->teamRepository->findBy(['project' => $team->getProject(), 'permission' => Team::STATUS_ROLE_MASTER, 'deleted' => false])) <= 1) {
            $this->addFlash('alert', "You couldn't be removed from this project. You're only master");

            return $this->redirectToRoute('dashboard_project', ['name' => $team->getProject()->getName()]);
        }

        $team->setDeleted(true)
            ->setInvitation(false)
            ->setPermission(null)
            ->setMessage('')
            ->setModified(new \DateTime());
        $this->entityManager->persist($team);
        $this->entityManager->flush();

        return $this->redirectToRoute('all_project');
    }
}
