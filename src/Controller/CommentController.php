<?php

namespace App\Controller;

use App\Entity\Comment;
use App\Entity\Task;
use App\Entity\Team;
use App\Form\CommentType;
use App\Service\PermissionService;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class CommentController extends AbstractController
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var PermissionService
     */
    private $permission;
    /**
     * @var RequestStack
     */
    private $requestStack;

    public function __construct(
        EntityManagerInterface $entityManager,
        PermissionService $permission,
        RequestStack $requestStack
    ) {
        $this->entityManager = $entityManager;
        $this->permission = $permission;
        $this->requestStack = $requestStack->getCurrentRequest();
    }

    /**
     * @Route("/new-comment/{task}", name="new_comment")
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function newComment(Task $task)
    {
        $this->permission->checkPermissions($task->getProject(), Team::STATUS_ROLE_DEVELOPER);

        $content = $this->requestStack->get('content_comment');

        if ($this->requestStack->isMethod('POST') && !empty($content)) {
            $comment = new Comment();
            $comment->setContent($content)
                ->setTask($task)
                ->setCreatedAt(new \DateTime())
                ->setAuthor($this->getUser());
            $comment->getTask()->getProject()->setModified(new \DateTime());

            $this->entityManager->persist($comment);
            $this->entityManager->flush();
        }

        return $this->redirectToRoute('table_task', [
            'name' => $task->getProject(),
            'id' => $task->getBoard()->getId(),
        ]);
    }

    /**
     * @Route("/remove-comment/{id}", name="remove_comment")
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function removeComment(Comment $comment)
    {
        if ($comment->getAuthor() !== $this->getUser()) {
            throw new AccessDeniedException();
        }

        $comment->setDeleted(true)
            ->setModified(new \DateTime());
        $comment->getTask()->getProject()->setModified(new \DateTime());
        $this->entityManager->persist($comment);
        $this->entityManager->flush();

        return $this->redirectToRoute('table_task', [
            'name' => $comment->getTask()->getProject(),
            'id' => $comment->getTask()->getBoard()->getId(),
        ]);
    }

    /**
     * @Route("/edit-comment/{id}", name="edit_comment")
     *
     * @Template("comment/edit.html.twig")
     *
     * @return null[]|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function editComment(Comment $comment)
    {
        if ($comment->getAuthor() !== $this->getUser()) {
            throw new AccessDeniedException();
        }

        $formComment = $this->createForm(CommentType::class, $comment);
        if ($this->requestStack->isMethod('POST')) {
            $formComment->handleRequest($this->requestStack);
            $comment->setModified(new \DateTime());
            $comment->getTask()->getProject()->setModified(new \DateTime());
            $this->entityManager->persist($comment);
            $this->entityManager->flush();

            return $this->redirectToRoute('table_task', [
                'name' => $comment->getTask()->getProject(),
                'id' => $comment->getTask()->getBoard()->getId(),
            ]);
        }

        return [
            'formComment' => isset($formComment) ? $formComment->createView() : null,
            'project' => $comment->getTask()->getProject(),
        ];
    }
}
