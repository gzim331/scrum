<?php

namespace App\Controller;

use App\Entity\Project;
use App\Entity\Team;
use App\Repository\ProjectRepository;
use App\Repository\TeamRepository;
use App\Service\PaginationService;
use App\Service\PermissionService;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\Annotation\Route;

class InvitationController extends AbstractController
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var PermissionService
     */
    private $permission;
    /**
     * @var PaginationService
     */
    private $pagination;
    /**
     * @var RequestStack
     */
    private $requestStack;
    /**
     * @var TeamRepository
     */
    private $teamRepository;
    /**
     * @var ProjectRepository
     */
    private $projectRepository;

    public function __construct(
        EntityManagerInterface $entityManager,
        PermissionService $permission,
        PaginationService $pagination,
        RequestStack $requestStack,
        TeamRepository $teamRepository,
        ProjectRepository $projectRepository
    ) {
        $this->entityManager = $entityManager;
        $this->permission = $permission;
        $this->pagination = $pagination;
        $this->requestStack = $requestStack->getCurrentRequest();
        $this->teamRepository = $teamRepository;
        $this->projectRepository = $projectRepository;
    }

    /**
     * @Route("/{name}/team/invitations", name="invitation_access_team")
     * @Template("invitation/invitation.html.twig")
     *
     * @return array
     */
    public function invitationAccess(Project $project)
    {
        $this->permission->checkPermissions($project, Team::STATUS_ROLE_MASTER);

        $selectTeam = $this->teamRepository->selectRequest($project);

        return [
            'selectTeam' => $this->pagination->paginate($selectTeam, $this->requestStack),
            'project' => $project,
        ];
    }

    /**
     * @Route("/join/{id}", name="join_to_team")
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function joinToTeam(Project $project)
    {
        $message = '';

        if ($this->requestStack->getMethod('POST') && $this->requestStack->get('invitation_submit')) {
            $message = $this->requestStack->get('invitation_message');
        }

        /** @var Team $team */
        if ($team = $this->teamRepository->findOneBy(['project' => $project, 'participant' => $this->getUser()])) {
            $team->setMessage($message)
                ->setInvitation(true)
                ->setModified(new \DateTime())
                ->setDeleted(false);
        } else {
            $team = new Team();
            $team->setProject($project)
            ->setParticipant($this->getUser())
            ->setPermission(null)
            ->setMessage($message)
            ->setCreatedAt(new \DateTime())
            ->setInvitation(true);
        }
        try {
            $this->entityManager->persist($team);
            $this->entityManager->flush();
        } catch (\Exception $exception) {
            $this->addFlash('alert', 'Message is too long');
        }

        return $this->redirectToRoute('dashboard_project', ['name' => $project->getName()]);
    }

    /**
     * @Route("/join-confirm/{id}", name="join_confirm")
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function joinConfirm(Team $team)
    {
        $this->permission->checkPermissions($team->getProject(), Team::STATUS_ROLE_MASTER);

        $team->setInvitation(false)
            ->setModified(new \DateTime());
        $this->entityManager->persist($team);
        $this->entityManager->flush();

        return $this->redirectToRoute('index_team', ['name' => $team->getProject()->getName()]);
    }

    /**
     * @Route("/cancel-join/{project}/{user}", name="cancel_join")
     */
    public function cancelJoin($project, $user)
    {
        $team = $this->teamRepository->findOneBy(['project' => $project, 'participant' => $user, 'deleted' => false]);

        if ($team) {
            $team->setDeleted(true)
                ->setModified(new \DateTime())
                ->setInvitation(false)
                ->setMessage('')
                ->setPermission(null);
            $this->entityManager->persist($team);
            $this->entityManager->flush();
        }

        return $this->redirectToRoute('all_project');
    }
}
