<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\SettingsType;
use App\Form\UserType;
use App\Repository\ProjectRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class UserController extends AbstractController
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var UserRepository
     */
    private $userRepository;
    /**
     * @var ProjectRepository
     */
    private $projectRepository;

    public function __construct(EntityManagerInterface $entityManager, UserRepository $userRepository, ProjectRepository $projectRepository)
    {
        $this->entityManager = $entityManager;
        $this->userRepository = $userRepository;
        $this->projectRepository = $projectRepository;
    }

    /**
     * @Route("/welcome", name="login")
     *
     * @Template("user/login.html.twig")
     *
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function login(AuthenticationUtils $authenticationUtils)
    {
        if (true === $this->get('security.authorization_checker')->isGranted('ROLE_USER')) {
            return $this->redirectToRoute('all_project');
        }

        $error = $authenticationUtils->getLastAuthenticationError();
        $lastUsername = $authenticationUtils->getLastUsername();

        return [
            'error' => $error,
            'username' => $lastUsername,
        ];
    }

    /**
     * @Route("/register", name="register")
     *
     * @Template("user/register.html.twig")
     *
     * @return null[]|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function register(Request $request, UserPasswordEncoderInterface $encoder)
    {
        if (true === $this->get('security.authorization_checker')->isGranted('ROLE_USER')) {
            return $this->redirectToRoute('all_project');
        }

        $user = new User();
        $registerForm = $this->createForm(UserType::class, $user);

        if ($request->isMethod('POST')) {
            $registerForm->handleRequest($request);
            if ($registerForm->isSubmitted() && $registerForm->isValid()) {
                $user->setPassword($encoder->encodePassword($user, $user->getPassword()))
                    ->setPath('default.png');
                $this->entityManager->persist($user);
                $this->entityManager->flush();
                $this->addFlash('success', 'Welcome to ScruMaster');

                return $this->redirectToRoute('login');
            } else {
                $this->addFlash('alert', 'User cannot be created');
            }
        }

        return [
            'registerForm' => isset($registerForm) ? $registerForm->createView() : null,
        ];
    }

    /**
     * @Route("/user/{username}", name="profile")
     *
     * @Template("user/profile.html.twig")
     *
     * @return array
     */
    public function profile(User $user)
    {
        return [
            'infoAboutUser' => $this->userRepository->findInfo($user),
            'selectProject' => $this->projectRepository->selectAllProject($user),
            'selectAuthorProject' => $this->projectRepository->selectUserProject($user),
        ];
    }

    /**
     * @Route("/settings/{id}", name="settings")
     *
     * @Template("user/settings.html.twig")
     *
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function settings(Request $request, User $user, UserPasswordEncoderInterface $encoder)
    {
        if ($this->getUser() != $user->getUsername()) {
            throw new AccessDeniedException();
        }

        $oldUserName = $user->getUsername();
        $formSettings = $this->createForm(SettingsType::class, $user);
        if ($request->isMethod('POST')) {
            $usernameFromForm = $request->request->get('settings')['username'];
            if (
                $oldUserName !== $usernameFromForm
                && $this->entityManager->getRepository(User::class)->checkUsernameExists($usernameFromForm)
            ) {
                $this->addFlash('alert', 'Username must be unique');

                return $this->redirectToRoute('settings', ['id' => $this->getUser()->getId()]);
            }

            $formSettings->handleRequest($request);
            $user->upload();
            $user->setPassword($encoder->encodePassword($user, $user->getPassword()));
            $user->setEnable(true);
            $this->entityManager->persist($user);
            $this->entityManager->flush();
            $this->addFlash('success', 'The information has been updated');

            return $this->redirectToRoute('settings', ['id' => $this->getUser()->getId()]);
        }

        return [
            'form' => isset($formSettings) ? $formSettings->createView() : null,
            'user' => $user,
        ];
    }

    /**
     * @Route("/logout", name="logout")
     */
    public function logout()
    {
    }

    /**
     * @Route("/settings/remove/user/{id}", name="settings_remove_user")
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function remove(User $user)
    {
        if ($this->getUser() != $user->getUsername()) {
            throw new AccessDeniedException();
        }

        $user->setEnable(false);
        $this->entityManager->persist($user);
        $this->entityManager->flush();

        return $this->redirectToRoute('logout');
    }
}
