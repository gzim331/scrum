<?php

namespace App\Controller;

use App\Entity\Category;
use App\Entity\Project;
use App\Entity\Team;
use App\Form\CategoryType;
use App\Repository\CategoryRepository;
use App\Repository\TaskRepository;
use App\Repository\TeamRepository;
use App\Service\PaginationService;
use App\Service\PermissionService;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints\DateTime;

class CategoryController extends AbstractController
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var TeamRepository
     */
    private $teamRepository;
    /**
     * @var PermissionService
     */
    private $permission;
    /**
     * @var PaginationService
     */
    private $pagination;
    /**
     * @var RequestStack
     */
    private $requestStack;
    /**
     * @var CategoryRepository
     */
    private $categoryRepository;
    /**
     * @var TaskRepository
     */
    private $taskRepository;

    public function __construct(
        EntityManagerInterface $entityManager,
        TeamRepository $teamRepository,
        PermissionService $permission,
        PaginationService $pagination,
        RequestStack $requestStack,
        CategoryRepository $categoryRepository,
        TaskRepository $taskRepository
    ) {
        $this->entityManager = $entityManager;
        $this->teamRepository = $teamRepository;
        $this->permission = $permission;
        $this->pagination = $pagination;
        $this->requestStack = $requestStack->getCurrentRequest();
        $this->categoryRepository = $categoryRepository;
        $this->taskRepository = $taskRepository;
    }

    /**
     * @Route("/{name}/category", name="index_category")
     * @Template("category/index.html.twig")
     *
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function index(Project $project)
    {
        $this->permission->checkPermissions($project, Team::STATUS_ROLE_MODERATOR);

        $category = new Category();
        $formCategory = $this->createForm(CategoryType::class, $category);

        if ($this->requestStack->isMethod('POST')) {
            $formCategory->handleRequest($this->requestStack);
            if ($formCategory->isValid() &&
                !$this->categoryRepository->findCategoryByName($formCategory->getData()->getName(), $project)
            ) {
                $category->setProject($project)
                    ->setCreatedAt(new \DateTime());
                $project->setModified(new \DateTime());
                $this->entityManager->persist($category);
                $this->entityManager->persist($project);
                $this->entityManager->flush();

                return $this->redirectToRoute('index_category', ['name' => $project->getName()]);
            } else {
                $this->addFlash('alert', "Category couldn't be added");
            }
        }

        $findCategoryByProject = $this->categoryRepository->findCategoryByProject($project);

        return [
            'formCategory' => isset($formCategory) ? $formCategory->createView() : null,
            'selectCategory' => $this->pagination->paginate($findCategoryByProject, $this->requestStack),
            'project' => $project,
        ];
    }

    /**
     * @Route("/edit/category/{id}", name="edit_category")
     * @Template("category/edit.html.twig")
     *
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function edit(Category $category)
    {
        $this->permission->checkPermissions($category->getProject(), Team::STATUS_ROLE_MODERATOR);

        if ($category->getDeleted()) {
            return $this->redirectToRoute('index_category', ['name' => $category->getProject()]);
        }

        $formCategory = $this->createForm(CategoryType::class, $category);
        if ($this->requestStack->isMethod('POST')) {
            $formCategory->handleRequest($this->requestStack);
            if (!$this->categoryRepository->findCategoryByName($formCategory->getData()->getName(), $category->getProject())) {
                $category->setModified(new \DateTime());
                $category->getProject()->setModified(new \DateTime());
                $this->entityManager->persist($category);
                $this->entityManager->flush();

                return $this->redirectToRoute('index_category', ['name' => $category->getProject()->getName()]);
            } else {
                $this->addFlash('alert', "Category couldn't be edit");
            }
        }

        return [
            'formCategory' => isset($formCategory) ? $formCategory->createView() : null,
            'project' => $category->getProject(),
        ];
    }

    /**
     * @Route("/category_delete/{id}", name="delete_category")
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function delete(Category $category)
    {
        $this->permission->checkPermissions($category->getProject(), Team::STATUS_ROLE_MODERATOR);

        $tasks = $this->taskRepository->findTasksByCategory($category);
        foreach ($tasks as $task) {
            $task->setDeleted(true)
                ->setModified(new DateTime());
            $this->entityManager->persist($task);
        }

        $category->setDeleted(true)
            ->setModified(new \DateTime());
        $category->getProject()->setModified(new \DateTime());
        $this->entityManager->persist($category);
        $this->entityManager->flush();

        return $this->redirectToRoute('index_category', ['name' => $category->getProject()]);
    }
}
