<?php

namespace App\Controller;

use App\Entity\Board;
use App\Entity\Project;
use App\Entity\Team;
use App\Form\BoardType;
use App\Repository\BoardRepository;
use App\Repository\TaskRepository;
use App\Repository\TeamRepository;
use App\Service\PaginationService;
use App\Service\PermissionService;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class BoardController extends AbstractController
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var TeamRepository
     */
    private $teamRepository;
    /**
     * @var PermissionService
     */
    private $permission;
    /**
     * @var PaginationService
     */
    private $pagination;
    /**
     * @var RequestStack
     */
    private $requestStack;
    /**
     * @var TaskRepository
     */
    private $taskRepository;
    /**
     * @var BoardRepository
     */
    private $boardRepository;

    public function __construct(
        EntityManagerInterface $entityManager,
        TeamRepository $teamRepository,
        PermissionService $permission,
        PaginationService $pagination,
        RequestStack $requestStack,
        TaskRepository $taskRepository,
        BoardRepository $boardRepository
    ) {
        $this->entityManager = $entityManager;
        $this->teamRepository = $teamRepository;
        $this->permission = $permission;
        $this->pagination = $pagination;
        $this->requestStack = $requestStack->getCurrentRequest();
        $this->taskRepository = $taskRepository;
        $this->boardRepository = $boardRepository;
    }

    /**
     * @Route("/{name}/board", name="index_board")
     * @Template("board/index.html.twig")
     *
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function index(Project $project)
    {
        $this->permission->checkPermissions($project, Team::STATUS_ROLE_MODERATOR);

        $board = new Board();
        $formBoard = $this->createForm(BoardType::class, $board);
        $boards = $this->boardRepository->findBoardByProject($project);

        if ($this->requestStack->isMethod('POST')) {
            $formBoard->handleRequest($this->requestStack);
            $boardName = $formBoard->getData()->getName();
            if ($formBoard->isValid() && null != $boardName &&
                (false === $this->boardRepository->checkBoardExist($boardName, $project) ||
                false === $this->boardRepository->isBoardUndeleted($boardName, $project))
            ) {
                if (count($boards) > 30 && false === $formBoard->getData()->getHidden()) {
                    $this->addFlash('alert', 'There cannot be more than 30 boards in a project. At the moment you can create it hidden or hide one of the old boards.');
                } else {
                    $board->setProject($project)
                        ->setCreatedAt(new \DateTime());

                    if (null === $formBoard->getData()->getSequence()) {
                        $board->setSequence(count($boards) + 1);
                    }

                    $project->setModified(new \DateTime());

                    $this->entityManager->persist($board);
                    $this->entityManager->persist($project);
                    $this->entityManager->flush();
                }

                return $this->redirectToRoute('index_board', ['name' => $project->getName()]);
            } else {
                $this->addFlash('alert', "Board couldn't be created");
            }
        }

        $findBoardByProject = $this->boardRepository->findBoardByProject($project);

        return [
            'formBoard' => isset($formBoard) ? $formBoard->createView() : null,
            'selectBoards' => $this->pagination->paginate($findBoardByProject, $this->requestStack),
            'project' => $project,
            'suggestionSequence' => count($boards) + 1,
        ];
    }

    /**
     * @Route("/edit/board/{id}", name="edit_board")
     * @Template("board/edit.html.twig")
     *
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function edit(Board $board)
    {
        $this->permission->checkPermissions($board->getProject(), Team::STATUS_ROLE_MODERATOR);

        if ($board->getDeleted()) {
            return $this->redirectToRoute('index_board', ['name' => $board->getProject()]);
        }

        $formBoard = $this->createForm(BoardType::class, $board);
        if ($this->requestStack->isMethod('POST')) {
            $formBoard->handleRequest($this->requestStack);

            if (count($this->boardRepository->findBoardByProject($board->getProject())) > 30
                && false === $formBoard->getData()->getHidden()) {
                $this->addFlash('alert', 'There cannot be more than 30 boards in a project. At the moment you can create it hidden or hide one of the old boards.');
            } else {
                $board->setModified(new \DateTime());
                $board->getProject()->setModified(new \DateTime());

                if (null === $formBoard->getData()->getSequence()) {
                    $this->addFlash('alert', 'Invalid sequence value');

                    return $this->redirectToRoute('edit_board', ['id' => $board->getId()]);
                }

                $this->entityManager->persist($board);
                $this->entityManager->flush();

                return $this->redirectToRoute('index_board', ['name' => $board->getProject()->getName()]);
            }
        }

        return [
            'formBoard' => isset($formBoard) ? $formBoard->createView() : null,
            'project' => $board->getProject(),
        ];
    }

    /**
     * @Route("/board_delete/{id}", name="delete_board")
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function delete(Board $board)
    {
        $this->permission->checkPermissions($board->getProject(), Team::STATUS_ROLE_MODERATOR);

        if (null != $this->taskRepository->findTasksByBoard($board)) {
            $this->addFlash('alert', 'There are tasks in this board');
        } elseif ('Backlog' === $board->getName()) {
            $this->addFlash('alert', "'Backlog' board couldn't be removed");
        } else {
            $board->setDeleted(true)
                ->setModified(new \DateTime());
            $board->getProject()->setModified(new \DateTime());
            $this->entityManager->persist($board);
            $this->entityManager->flush();
        }

        return $this->redirectToRoute('index_board', ['name' => $board->getProject()]);
    }
}
