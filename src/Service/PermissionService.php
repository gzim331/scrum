<?php

namespace App\Service;

use App\Entity\Team;
use App\Repository\TeamRepository;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class PermissionService
{
    /**
     * @var TeamRepository
     */
    private $teamRepository;
    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    public function __construct(TeamRepository $teamRepository, TokenStorageInterface $tokenStorage)
    {
        $this->teamRepository = $teamRepository;
        $this->tokenStorage = $tokenStorage->getToken()->getUser();
    }

    public function checkPermissions($project, $status)
    {
        $result = false;

        $developer = $this->teamRepository->isStatusDeveloper($project, $this->tokenStorage);
        $moderator = $this->teamRepository->isStatusModerator($project, $this->tokenStorage);
        $master = $this->teamRepository->isStatusMaster($project, $this->tokenStorage);

        switch ($status) {
            case Team::STATUS_ROLE_DEVELOPER:
                if ($developer || $moderator || $master) {
                    $result = true;
                }
                break;
            case Team::STATUS_ROLE_MODERATOR:
                if ($moderator || $master) {
                    $result = true;
                }
                break;
            case Team::STATUS_ROLE_MASTER:
                if ($master) {
                    $result = true;
                }
                break;
        }

        if (false === $result) {
            throw new AccessDeniedException();
        }
    }
}
