<?php

namespace App\Form;

use App\Entity\Board;
use App\Entity\Category;
use App\Entity\Task;
use App\Repository\BoardRepository;
use App\Repository\CategoryRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TaskType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $project = $options['project'];
        $builder
            ->add('title')
            ->add('category', EntityType::class, [
                'class' => Category::class,
                'placeholder' => '---',
                'query_builder' => function (CategoryRepository $er) use ($project) {
                    return $er->createQueryBuilder('c')
                        ->where('c.deleted = :status')
                        ->setParameter('status', false)
                        ->andWhere('c.project = :project')
                        ->setParameter('project', $project)
                        ->orderBy('c.name', 'ASC');
                },
                'choice_label' => function ($type) {
                    return $type->getName();
                },
                'required' => false,
            ])
            ->add('status', ChoiceType::class, [
                'choices' => [
                    'ICE BOX' => Task::STATUS_ICEBOX,
                    'IN PROGRESS' => Task::STATUS_INPROGRESS,
                    'TESTING' => Task::STATUS_TESTING,
                    'COMPLETE' => Task::STATUS_COMPLETE,
                ],
            ])
            ->add('description', TextareaType::class)
            ->add('board', EntityType::class, [
                'class' => Board::class,
                'placeholder' => '---',
                'query_builder' => function (BoardRepository $br) use ($project) {
                    return $br->createQueryBuilder('b')
                        ->where('b.deleted = :status')
                        ->setParameter('status', false)
                        ->andWhere('b.project = :project')
                        ->setParameter('project', $project)
                        ->orderBy('b.name', 'DESC');
                },
                'choice_label' => function ($type) {
                    return $type->getName();
                },
                'required' => true,
            ])
            ->add('add', SubmitType::class, ['label' => 'Save'])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Task::class,
            'project' => null,
        ]);
    }
}
